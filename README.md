# Class Computer Sciences - Assignment 4

Symbolic Math Toolbox of MATLAB
Solving a plane Truss.

## Getting Started

Starte mit den Einführungen:

* solution.mlx 

### Prerequisites

MATLAB/Simulink base installation (2020b) including Symbolic Math Toolbox

### Installing

-

## Built With

* [MATLAB](http://www.mathworks.de) - MATLAB/Simulink

## Versioning

Für weitere Versionen siehe [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Rüdiger Götting** - *Initial work* - [Mail](mailto:ruediger.goetting@hs-emden-leer.de)

Weitere Mitarbeit: [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

-

## Acknowledgments

-
